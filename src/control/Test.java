package control;

import model.BankAccount;
import model.Company;
import model.Country;
import model.Data;
import model.Person;
import model.Product;
import model.TaxCalculator;
import interfaces.Measurable;
import interfaces.Taxable;

public class Test {
	
	public static void main(String[] args){
		Test test = new Test();
		test.testPerson();
		System.out.println("------------------------------------");
		test.testMin();
		System.out.println("------------------------------------");
		test.testTax();
	}
	//First answer
	public void testPerson(){
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("John",180, 100000);
		persons[1] = new Person("Mary",165, 250000);
		persons[2] = new Person("Rick",185, 500000);
		System.out.println("Average height : " + Data.average(persons));
	}
	//Second answer
	public void testMin(){
		Measurable[] bankAccount = new Measurable[2];
		Measurable[] country = new Measurable[2];
		Measurable[] person = new Measurable[2];
		Measurable[] average = new Measurable[3];
		
		
		person[0] = new Person("John",180, 100000);
		person[1] = new Person("Mary",165, 250000);
		
		country[0] = new Country("Thailand", 10000);
		country[1] = new Country("China", 60000);
		
		bankAccount[0] = new BankAccount("a", 5000);
		bankAccount[1] = new BankAccount("b", 6000);
		
		average[0] = Data.min(person[0], person[1]);
		average[1] = Data.min(country[0], country[1]);
		average[2] = Data.min(bankAccount[0], bankAccount[1]);
		
		System.out.println("Average height : " + Data.average(person));
		System.out.println("Average population : " +  Data.average(country));
		System.out.println("Average balance : " + Data.average(bankAccount));
		System.out.println("Average : " + Data.average(average));
	}
	//Third answer
	public void testTax(){
		Person[] persons = new Person[2];
		Company[] company = new Company[2];
		Product[] product = new Product[2];
		Taxable[] taxable = new Taxable[3];
		
		persons[0] = new Person("John", 180, 100000);
		persons[1] = new Person("Mary", 165, 250000);
		
		company[0] = new Company("AA", 5000000, 100000);
		company[1] = new Company("BB", 4000000, 200000);
		
		product[0] = new Product("item1", 500);
		product[1] = new Product("item2", 1000);
		
		taxable[0] = persons[0];
		taxable[1] = company[0];
		taxable[2] = product[0];
		
		System.out.println("Person sum tax : " + TaxCalculator.amount(persons));
		System.out.println("Company sum tax : " + TaxCalculator.amount(company));
		System.out.println("Product sum tax: " + TaxCalculator.amount(product));
		System.out.println("Taxable sum tax : " + TaxCalculator.amount(taxable));
	}

}
