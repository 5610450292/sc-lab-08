package model;

import interfaces.Taxable;

public class TaxCalculator {
	
	public static double amount(Taxable[] taxlist) {
		double sum = 0;
		for (Taxable tax : taxlist) {
			sum = sum + tax.getTax();
		}
		return sum;
	}

}
