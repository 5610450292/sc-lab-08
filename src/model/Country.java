package model;

import interfaces.Measurable;

public class Country implements Measurable{

	String name;
	double population;
	
	public Country(String name, double population) {
		this.name = name;
		this.population = population;
	}
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.population;
	}
	
	

}
