package model;

import interfaces.Taxable;

public class Company implements Taxable {
	
	String name;
	double income;
	double payout;

	public Company(String name, double income, double payout) {
		this.name = name;
		this.income = income;
		this.payout = payout;
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double tax = 0;
		if (income - payout >= 0) {
			tax = (income - payout) * 0.3;
		}
		return tax;
	}

}
