package model;

import interfaces.Measurable;

public class BankAccount implements Measurable{

	String name;
	double balance;
	
	public BankAccount(String name, double balance) {
		this.name = name;
		this.balance = balance;
	}
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.balance;
	}

}
