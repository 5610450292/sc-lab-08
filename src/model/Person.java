package model;

import interfaces.Measurable;
import interfaces.Taxable;

public class Person implements Measurable,Taxable{

	String name ;
	double height;
	double salary;	
	
	public Person(String name, double height, double salary) {
		this.name = name;
		this.height = height;
		this.salary = salary;
	}
	public String toString(){
		return name + " " + height;
	}
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.height;
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double tax = 0;
		double annual = 0;
		if (Math.floor(salary) <= 300000){
			tax = this.salary * 0.05;
		}
		else {
			tax = 300000 * 0.05;
			annual = this.salary - 300000;
			tax = tax + (annual * 0.1);
		}
		return tax;
	}
	

}
